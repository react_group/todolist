import {useState} from "react";
import './App.css';
import ToDoList from "./ToDoList/ToDoList";
import ToDoInput from "./ToDoInput/ToDoInput";


function App() {

    const [tasks, setTasks] = useState(
        [
            {id: 1, text: 'Create To Do list', status: false},
            {id: 2, text: 'Create remove button', status: true},
            {id: 3, text: 'Create input for add new task', status: false},
            {id: 4, text: 'Create checkbox', status: false},
        ]
    );


    const checkTask = (id, status) => {
        const taskCopy = tasks.map(task => {
            if (task.id === id) {
                task.status = !status
            }

            return task
        })

        setTasks(taskCopy);
    }


    const tasksCopy = tasks.map(task => (
        <ToDoList
            key = {task.id}
            text = {task.text}
            removeTask = {() => removeTask(task.id)}
            status = {task.status}
            checkTask = {() => checkTask(task.id, task.status)}
        />
    ));


    const removeTask = id => {
        setTasks(tasks.filter(task => task.id !== id));
    }


    const addTask = (value) => {
        if (value !== '') {
            setTasks([...tasks, {
                id: Math.floor(Math.random() * 100),
                text: value,
                status: false
            }]);
        } else {
            alert('Enter A message!')
        }
    }


    return (
        <div className="App">
            <ToDoInput
                addTask = {e => addTask(e.target.parentElement.previousElementSibling.value)}
            />

            <div className="task-list">
                <div className="container w-75">
                    <ul className="list-group">
                        {tasksCopy}
                    </ul>
                </div>
            </div>
        </div>
    );
}

export default App;

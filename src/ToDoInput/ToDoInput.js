import React from 'react';


const ToDoInput = (props) => {

    return (
        <header className="header">
            <div className="container">
                <div className="input-group mb-3 mt-5">
                    <input id="addTaskInput" type="text" className="form-control" placeholder="Add new task"
                           aria-label="Recipient's username" aria-describedby="button-addon2"
                        />
                    <div className="input-group-append">
                        <button className="btn btn-outline-secondary" type="button" id="button-addon2"
                        onClick={props.addTask}
                        >Add Task</button>
                    </div>
                </div>
            </div>
        </header>
    );
};

export default ToDoInput;
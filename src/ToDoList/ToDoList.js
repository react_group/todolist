import React from 'react';
import './ToDoList.css';


const ToDoList = (props) => {
    return (
        <li className="list-group-item d-flex justify-content-start align-items-center">
            <input type="checkbox" className="taskStatus mr-4"
                checked={props.status}
                onChange={props.checkTask}
            />
            <span>{props.text}</span>
            <button type="button" className="btn btn-danger ml-auto" onClick={props.removeTask}>Delete</button>
        </li>
    );
};

export default ToDoList;